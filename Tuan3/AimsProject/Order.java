package next.ducthuan;

public class Order {
	private static final int MAX_NUMBERS_ORDER =10;
	private DigitalVideoDisc itemsOrdered[]= new DigitalVideoDisc[MAX_NUMBERS_ORDER];
	private int qtyOrdered=0, i, j;
	private float gia=0;
	public void setQtyOrdered(int qtyOrdered)
	{
		this.qtyOrdered=qtyOrdered;
	}
	public int getQtyOrdered()
	{
		return qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(qtyOrdered<10)
		{
			itemsOrdered[qtyOrdered]=disc;
			//qtyOrdered+=1;
			System.out.print(itemsOrdered[qtyOrdered].getTitle()+" da duoc them\n");
			qtyOrdered+=1;
		}
		else
		{
			System.out.print(itemsOrdered[qtyOrdered].getTitle()+" khong duoc them\n");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		for(i=0;i<qtyOrdered;i++)
		{
			if(itemsOrdered[i]==disc)
			{
				for(j=i;j<qtyOrdered;j++)
				{
					itemsOrdered[j]=itemsOrdered[j+1];
				}
				qtyOrdered-=1;
			}
		}
		System.out.print(disc.getTitle()+" da duoc xoa\n");
	}
	public float totalCost()
	{
		for(i=0;i<qtyOrdered;i++)
		{
			gia=gia+itemsOrdered[i].getCost();
		}
		return gia;
	}

}
