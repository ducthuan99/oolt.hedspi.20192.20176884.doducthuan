package hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc.*;
import java.util.Random;
public class Order {
	private static final int MAX_NUMBERS_ORDER =4;
	private static final int MAX_LIMITTED_ORDERS=5;
	private static int nbOrders=0;
	private DigitalVideoDisc itemsOrdered[]= new DigitalVideoDisc[MAX_NUMBERS_ORDER];
	private int qtyOrdered=0, i, j;
	private float gia;
	private String date;
	public String getdate()
	{
		return date;
	}
	public void setdate(String date)
	{
		this.date=date;
	}
	public void setQtyOrdered(int qtyOrdered)
	{
		this.qtyOrdered=qtyOrdered;
	}
	public int getQtyOrdered()
	{
		return qtyOrdered;
	}
	public void show()
	{
		for(int i=0;i<qtyOrdered;i++)
		{
			System.out.println(itemsOrdered[i].getTitle());
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(qtyOrdered<MAX_NUMBERS_ORDER)
		{
			itemsOrdered[qtyOrdered]=disc;
			//qtyOrdered+=1;
			System.out.print(itemsOrdered[qtyOrdered].getTitle()+" da duoc them\n");
			qtyOrdered+=1;
		}
		else
		{
			System.out.print(itemsOrdered[qtyOrdered].getTitle()+" khong duoc them\n");
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc []dvdList, int lengthDvdList)
	{
		for(int i=0;i<lengthDvdList;i++)
		{
			if(qtyOrdered<MAX_NUMBERS_ORDER)
			{
				itemsOrdered[qtyOrdered]=dvdList[i];
				qtyOrdered++;
				System.out.println("Ban da co "+qtyOrdered+" mat hang!! So luong toi da co the mua la "+MAX_NUMBERS_ORDER);
			}
		}
		if(qtyOrdered>=MAX_NUMBERS_ORDER)
		{
			System.out.println("Ban da mua du so luong gioi han!!!-Khong the mua tiep!!!");
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2)
	{
		if(qtyOrdered<MAX_NUMBERS_ORDER)
		{
			itemsOrdered[qtyOrdered]=dvd1;
			qtyOrdered++;
			if(qtyOrdered<MAX_NUMBERS_ORDER)
			{
				itemsOrdered[qtyOrdered]=dvd2;
				qtyOrdered++;
			}
			else
			{
				System.out.println(dvd2.getTitle()+" khong the them");
			}
		}
		else
		{
			System.out.println(dvd1.getTitle()+" va " +dvd2.getTitle()+" khong the them!!!");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		for(i=0;i<qtyOrdered;i++)
		{
			if(itemsOrdered[i]==disc)
			{
				for(j=i;j<qtyOrdered;j++)
				{
					itemsOrdered[j]=itemsOrdered[j+1];
				}
				qtyOrdered-=1;
			}
		}
		System.out.print(disc.getTitle()+" da duoc xoa\n");
	}
	public float totalCost()
	{
		float gia=0;
		for(i=0;i<qtyOrdered;i++)
		{
			gia=gia+itemsOrdered[i].getCost();
		}
		return gia;
	}
	public void OrderDetail()
	{
		System.out.println("*********************Order************************");
		System.out.println("Date:"+ getdate());
		for (int i=0; i<qtyOrdered; i++) 
		{
			System.out.println(i+1+"." +  itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": " + itemsOrdered[i].getCost() + "$");
		}
		this.gia=totalCost();
		System.out.println("Total cost: "+this.gia);
		System.out.println("**************************************************");
	}
	public void getALuckyItem() 
	{
		Random rand = new Random();
		int rand_int = rand.nextInt(qtyOrdered);
		DigitalVideoDisc FreeItem = itemsOrdered[rand_int];
		System.out.println("Free lucky Item: "+ FreeItem.getTitle());
		gia = gia - FreeItem.getCost();
		itemsOrdered[rand_int].setTitle(FreeItem.getTitle() + "***");
		itemsOrdered[rand_int].setCost(0);
	}

}
