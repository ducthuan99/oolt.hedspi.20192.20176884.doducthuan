package hust.soict.hedspi.aims.media;
//public class DigitalVideoDisc extends Media;
//public class Book extends Media;

public class Media {
	String title;
	String category;
	float cost;
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public Media() {
		// TODO Auto-generated constructor stub
	}

}
