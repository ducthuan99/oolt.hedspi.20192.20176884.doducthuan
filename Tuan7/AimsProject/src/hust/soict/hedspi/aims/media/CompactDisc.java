package hust.soict.hedspi.aims.media;
import java.util.ArrayList;
public class CompactDisc extends Media implements Playable {
	private int length;
	private String artist;
	private ArrayList<Track> tracks;

	public CompactDisc(String artist, int length) {
		this.artist = artist;
		this.length = length;
		
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public ArrayList<Track> getTracks() {
		return tracks;
	}

	public void setTracks(ArrayList<Track> tracks) {
		this.tracks = tracks;
	}
	
	public void addTrack(Track track) {
		if (tracks.contains(track)) {
			System.out.println("Track duplicated!");
		} else {
			tracks.add(track);			
		}
	}
	
	public void removeTrack(Track track) {
		if (tracks.contains(track)) {
			tracks.remove(track);	
			
		} else {
			System.out.println("Track deleted!");		
		}
	}
	
	public int getAllLength() {
		int sum = 0;
		for (int i = 0; i < tracks.size(); ++i)
		{
		    sum = sum + tracks.get(i).getLength();
		}
		return sum;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		for (int i = 0; i < tracks.size(); ++i)
		{
			System.out.println("Playing Track: " + tracks.get(i).getTitle());
			System.out.println("Track length: " + tracks.get(i).getLength());
		    
		}
	}
}
