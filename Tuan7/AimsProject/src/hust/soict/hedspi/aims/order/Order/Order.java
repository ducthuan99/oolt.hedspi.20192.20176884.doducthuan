package hust.soict.hedspi.aims.order.Order;
import java.util.ArrayList;
import java.util.Random;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
public class Order {
	private static final int MAX_NUMBERS_ORDER =10;
	private static final int MAX_LIMITTED_ORDERS=5;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	//private int size=itemsOrdered.size();
	private static int nbOrders=0;
	private int i, j;
	private float gia;
	private String date;
	public String getdate()
	{
		return date;
	}
	public void setdate(String date)
	{
		this.date=date;
	}
	public void addMedia(DigitalVideoDisc disc) {
		if(itemsOrdered.size()< MAX_NUMBERS_ORDER) {
			itemsOrdered.add(disc);
			int size=itemsOrdered.size()-1;
			System.out.println(itemsOrdered.get(size).getTitle()+" da duoc them!");
		}
		else {
			System.out.println("Don hang day!!");
		}
	}
	public void addMedia(DigitalVideoDisc disc1, DigitalVideoDisc disc2) {
		if(itemsOrdered.size() < MAX_NUMBERS_ORDER) {
			itemsOrdered.add(disc1);
			int size=itemsOrdered.size()-1;
			System.out.println(itemsOrdered.get(size).getTitle()+" da duoc them!");
			if(itemsOrdered.size() < MAX_NUMBERS_ORDER) {
				itemsOrdered.add(disc2);
				int size2=itemsOrdered.size()-1;
				System.out.println(itemsOrdered.get(size2).getTitle()+" da duoc them!");
			}
			else {
				System.out.println("Don hang day!!");
			}
		}
		System.out.println("Don hang day!!");
	}
	public void removeMedia(DigitalVideoDisc disc) {
		if(itemsOrdered.contains(disc)) {
			itemsOrdered.remove(disc);
		}
		else {
			System.out.println("Khong tim thay!");
		}
	}
	public void removeMediaId(int id) {
		for(int i=0;i<itemsOrdered.size();i++) {
			if(itemsOrdered.get(i).getId()==id) {
				itemsOrdered.remove(i);
			}
		}
	}
	public float totalCost()
	{
		float gia=0;
		for(i=0;i<itemsOrdered.size();i++)
		{
			gia=gia+itemsOrdered.get(i).getCost();
		}
		return gia;
	}
	public void OrderDetail()
	{
		System.out.println("*********************Order************************");
		System.out.println("Date:"+ getdate());
		for (int i=0; i<itemsOrdered.size(); i++) 
		{
			System.out.println(i+1+"." +  itemsOrdered.get(i).getTitle() + " - " + itemsOrdered.get(i).getCategory() + ": " + itemsOrdered.get(i).getCost() + "$");
		}
		//this.gia=totalCost();
		//System.out.println("Total cost: "+this.gia);
		System.out.println("**************************************************");
	}
	public void getALuckyItem() 
	{
		Random rand = new Random();
		int rand_int = rand.nextInt(itemsOrdered.size());
		//DigitalVideoDisc FreeItem = itemsOrdered.get(rand_int);
		System.out.println("Free lucky Item: "+ itemsOrdered.get(rand_int).getTitle());
		gia = gia - itemsOrdered.get(rand_int).getCost();
		itemsOrdered.get(rand_int).setTitle(itemsOrdered.get(rand_int).getTitle() + "***");
		itemsOrdered.get(rand_int).setCost(0);
	}

}
