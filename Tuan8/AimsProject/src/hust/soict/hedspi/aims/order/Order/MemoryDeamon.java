package hust.soict.hedspi.aims.order.Order;

public class MemoryDeamon implements java.lang.Runnable{
	private long memoryUsed = 0;
	public void run() {
		// TODO Auto-generated method stub
		Runtime rt = Runtime.getRuntime();
		long used;
		
		while (true) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != memoryUsed) {
				System.out.println("\tMemory used = " + used);
				memoryUsed = used;
			}
		}
	}
}
