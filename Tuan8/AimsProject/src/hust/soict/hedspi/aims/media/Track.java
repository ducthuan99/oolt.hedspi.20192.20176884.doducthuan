package hust.soict.hedspi.aims.media;
public class Track implements Playable {
    private int length;
	private String title;
	public Track() {
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public void play() {	
	}	
}

