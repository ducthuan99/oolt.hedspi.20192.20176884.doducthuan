
package hust.soict.hedspi.aims.media;

public class DigitalVideoDisc extends Disc implements Playable {
	private String director;
	private int length;
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public DigitalVideoDisc(String title)
	{
		super();
		this.title=title;
	}
	public DigitalVideoDisc(String title, String category)
	{
		super();
		this.category=category;
	}
	public DigitalVideoDisc(String title, String category,String director)
	{
		super();
		this.director=director;
	}
	public DigitalVideoDisc(String title, String category,String director,int length)
	{
		super();
		this.length=length;
	}
	public DigitalVideoDisc(String title, String category,String director,int length,float cost)
	{
		super();
		this.cost=cost;
	}
	public DigitalVideoDisc(String title, String category,String director,int length,float cost, int id)
	{
		super();
		this.id=id;
	}
	public boolean search(String title) 
	{
		return this.title.indexOf(title) !=-1? true:false;
	}
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
}
