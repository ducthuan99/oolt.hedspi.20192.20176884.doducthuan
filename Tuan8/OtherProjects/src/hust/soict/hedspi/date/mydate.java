package hust.soict.hedspi.date;
import java.util.Calendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;  
import java.util.Date; 
import java.util.Scanner;
public class mydate {
	private int date,month,year;
	private String sdate, smonth, syear;
	private String stringdate;
	public String getsdate()
	{
		return sdate;
	}
	public void setsdate(String sdate)
	{
		this.sdate=sdate;
	}
	public String getsmonth()
	{
		return smonth;
	}
	public void setsmonth(String smonth)
	{
		this.smonth=smonth;
	}
	public String getsyear()
	{
		return syear;
	}
	public void setyear(String syear)
	{
		this.syear=syear;
	}
	public String setstringdate()
	{
		this.stringdate=String.valueOf(this.date)+"/"+String.valueOf(this.month)+"/"+String.valueOf(this.year);
		return this.stringdate;
	}
	private String chuoi;
	public void Mydate()
	{
		Calendar calendar=Calendar.getInstance();
		System.out.println("Ngay hien tai la:"+calendar.get(Calendar.DATE));
		System.out.println("Thang hien tai la:"+(calendar.get(Calendar.MONTH)+1));
		System.out.println("Nam hien tai la:"+calendar.get(Calendar.YEAR));
	}
	public void Mydate(int date,int month,int year)
	{
		setdate(date);
		setmonth(month);
		setyear(year);
	}
	public void Mydate(String chuoi)
	{
		this.chuoi=chuoi;
		System.out.println(this.chuoi);
	}
	public int getdate()
	{
		return date;
	}
	public void setdate(int date)
	{
		if(date>1 && date<32)
		{
			this.date=date;
		}
		else
		{
			System.out.println("Ngay khong hop le!!!");
		}
		//this.date=date;
	}
	public int getmonth()
	{
		return month;
	}
	public void setmonth(int month)
	{
		if(month>1 && month<13)
		{
			this.month=month;
		}
		else
		{
			System.out.println("Thang khong hop le!!!");
		}
	}
	public int getyear()
	{
		return year;
	}
	public void setyear(int year)
	{
		this.year=year;
	}
	public void show()
	{
		
		System.out.println("Ngay thang nam nhap vao la:");
		System.out.println(getdate()+"/"+getmonth()+"/"+getyear());
		System.out.println(getyear()+"-"+getmonth()+"-"+getdate());
		
	}
	public void accept()
	{
		System.out.println("Hay nhap theo dinh dang:Ngay .. thang .. nam ...:");
		Scanner ro=new Scanner(System.in);
		String rout=ro.nextLine();
		//ngay .. thang .. nam  
		this.date=Integer.parseInt(rout.substring(5,7));
		this.month=Integer.parseInt(rout.substring(14,16));
		this.year=Integer.parseInt(rout.substring(21,25));
	}
	public void print()
	{
		Calendar calen=Calendar.getInstance();
		System.out.println("Ngay hien tai la:"+calen.get(Calendar.DATE));
		System.out.println("Thang hien tai la:"+calen.get(Calendar.MONTH)+1);
		System.out.println("Nam hien tai la:"+calen.get(Calendar.YEAR));
	}
	public void sprint()
	{
		Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("EEEEE MMMMM yyyy");
        String strDate = formatter.format(date);
        System.out.println(strDate);
	}
	public void stringprint() throws ParseException
	{
		Date strDate = new SimpleDateFormat("dd/MM/yyyy").parse(this.stringdate);
        //String strDate = formatter.format(this.stringdate);
        System.out.println(strDate);
	}
	
}
